" Specify a directory for plugins (for Neovim: ~/.local/share/nvim/plugged)
call plug#begin('~/.vim/plugged')

Plug 'mattn/emmet-vim'
Plug 'easymotion/vim-easymotion'
Plug 'jiangmiao/auto-pairs'
Plug 'matze/vim-move'
Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-surround'
Plug 'luochen1990/rainbow'
Plug 'sheerun/vim-polyglot'
Plug 'crusoexia/vim-monokai'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
" Snippets
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'tomtom/tlib_vim'
Plug 'garbas/vim-snipmate'
Plug 'grvcoelho/vim-javascript-snippets'
" Linter
" Plug 'w0rp/ale'

" Initialize plugin system
" :PlugInstall
" :PlugClean
" :PlugUpgrade
" :PlugUpdate
"
call plug#end()

"----------------CONFIGURACIONES--------------------
let mapleader = ","                             "Leader cambiado a ,
":set relativenumber                              "Numero de lineas relativas a posicion actual
set number                                        "Numero de linea actual
let g:user_emmet_leader_key='<C-Z>'         "Cambiar a ctrl z leader para emmet
:set expandtab                                    "Cambiar a espacios en vez de tabs
:set tabstop=2                            "Espacios para tab
:set shiftwidth=2                         "Espacios para indentacion
set autoindent                            "Copia la misma indentacion de la ultima linea
:set breakindent                          "Respetar indentacion en misma linea multiple line                                                                             as, ejemplo: esta linea actual
set wildignore+=*/tmp/*,*.so,*.swp,*.zip  "Ignorar las extension en el plugin ctrlp.vim
let g:move_key_modifier = 'C'             "Usar ctrl jk para mover seleccion o linea para vim-move
let NERDTreeMinimalUI = 1                 "Quita los numeros de la ventana de NerdTree
let g:rainbow_active = 1                  "Activar plugin rainbow para parentesis
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git\|dist'
" Use deoplete.
let g:deoplete#enable_at_startup = 1

" Activar Tab para sugerencias de deoplete
" inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"

:set cursorcolumn                         "Remarcar la columna actual
:set clipboard=unnamed                    "Comparte misma clipboard Mac con VIM
set noerrorbells visualbell t_vb=         "Desactiva ringbell
:set autoread                             "Recarga archivos
"Abrir Nerdtree cuando se inicia vim sin archivo seleccionado
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif "Cerrar vim si solo NERDtree esta abierto

"Configuracion linter ALE
"set nocompatible
"filetype off
"let &runtimepath.=',~/.vim/bundle/ale'
"filetype plugin on

"----------------- REMAPS, ASIGNACIONES--------------
"Presionar jj es igual a presional Esc
:imap jj <Esc>
"Presionar s y escribir letra a buscar para el plugin easyMotion
nmap s <Plug>(easymotion-overwin-f)
"Presionar leader + k y abre el explorador(:Ex)
nnoremap <leader>k :Ex<CR>
"Ctrl n para abrir NERDtree
map <C-n> :NERDTreeToggle<CR>
"Leader + <CR> para insertar un enter entre 2 tags en html
inoremap <leader><CR> <CR><C-o>==<C-o>O
"Control + s guardar archivo, si esta en insert, guarda y sigue en insert
inoremap <C-s> <esc>:w<cr>a
nnoremap <C-s> :w<cr>
" Usar Control + P para FZF
map <C-p> :FZF<CR>

"----------------- Syntax, Theme--------------
"Color pmenu
"highlight Pmenu ctermfg=16 ctermbg=220 guifg=#547577 guibg=#000000
syntax on
" Monokai
colorscheme monokai
set t_Co=256  " vim-monokai now only support 256 colours in terminal.
